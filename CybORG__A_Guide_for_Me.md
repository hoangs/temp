# CybORG: A Guide for Me
**Purpose**: A written guide (probably informal) for anyone who has minimial to no experience in Python. Anyone like me. 

Please skip the sections that may be irrelevant to you (e.g. setting up Virtual Environments).

## Prerequisites
* Python 3.7.5

Python 3.7.5 is required for the CybORG environment to run on. If your system already has a newer version of python installed, then an option to help manage older python versions is `pyenv`. 

Please refer to the `pyenv` [documentation](https://github.com/pyenv/pyenv#choosing-the-python-version) for further guidance. 

**TODO**: Write up quick 'how to setup' pyenv

## Installing CybORG
For anyone reading this on other platforms you can find the link to the GitLab repository here (granted Toby Richer has given you the rightful power to access it)
```
https://gitlab.com/cyborg-owners/Autonomous-Cyber-Ops
```
<details>
<summary> <b> Setting up a python virtual environment</b> </summary>
<p>
</p>
I hear that setting up virtual environments is good practice. My example, is like with Skyrim mods. If there are too many mods installed, then the Skyrim game crashes. Therefore, like python libraries (mods), our python program can crash and we want to have different environments for each of project.  

<p>
</p>
To setup virtual environment on your Linux system. 
<p>
</p>

```
pip install venv
python -m venv myenv
``` 

Locating the file directory of your newly created virtual environment, activate it.
```
source myenv/bin/activate
```
This is where you will install CybORG in and any other python libraries in for this project.
</details>

<details>
<summary> <b> Git cloning the repository </b> </summary>
<p>
</p>
To download or clone the CybORG files onto your local computer you can run the following git command. Make sure you run the command in the chosen location you want the files downloaded to. 
<p>
</p>

```
git clone https://gitlab.com/cyborg-owners/Autonomous-Cyber-Ops.git
```
</details>


Locate to `Autonomous-Cyber-Ops` folder to install CybORG locally using `pip`.
```
# from the root Autonomous-Cyber-Ops directory
pip install -e .
```
<sub> **Pro tip:** Don't be like me. Don't forget the 'space' and 'period' after the '-e' in the command </sub>

You may need to install additional libraries to get CybORG running (I couldn't replicate it again with a fresh install but will investigate it further). Just `pip` install the necessary libraries as instructed when met with any errors. When prompted to install `grpc` you can `pip install grpcio` instead. 

## Running the Keyboard Agent
As someone who is relatively new to Cyber Security, the Keyboard Agent was a great tool to learn what an agent wanted to achieve in this CybORG environment. The Keyboard Agent will give a better idea of what actions are available for a reinforcement agent during a game. 

**TODO:** Debug why the other version is not working.

Please swap branches to `james_dev` to access the Keyboard Agent.

<details>
<summary><b> Swapping git branches </b></summary>
<p>
</p>
To swap git branches is like swapping to different versions of the CybORG code. To do so, locate to `Autonomous-Cyber-Ops` directory.

You can check which branch you are currently sitting by:
```
git status
```
And to change to `james_dev` branch:
```
git checkout james_dev
```
</details>

Locate the `TestKeyboard.py` file. 
```
Autonomous-Cyber-Ops/CybORG/Agents/SimpleAgents/TestKeyboard.py
```

Before running the script, lets run through what the scenario is and what the agent(you) is expected to achieve. The network diagram of the scenario is as below:

<img src="https://gitlab.com/hoangs/temp/-/raw/master/images/guide-for-me-draft-3.png" alt="drawing" width="625"/>

The agent will already have access to a host in the public subnet at the beginning of an episode and will have to perform tactics, techniques and procedures under the MITRE ATT&CK framework to exploit and gain user access to a host on the private subnet. From there, the agent will have to repeat the process, in addition to pivoting, to finally exploit the host on the secure subnet and capturing the flag.

<sub> **Note:** The current version of Keyboard Agent is missing it's celebration message when the flag is captured but just ask Max and he will celebrate for you. </sub>


Run `TestKeyboard.py` the python script. In the terminal you should recieve the initial observation of the scenario. 

<img src='https://gitlab.com/hoangs/temp/-/raw/master/images/initial_obs.png' alt='drawing' width='625'/>

You will also notice that the KeyboardAgent is waiting for a user input, where the input is tied with the corresponding integer for said actions. Type the action you would like to execute and press 'Enter' to see the results. 

<img src='https://gitlab.com/hoangs/temp/-/raw/master/images/action_selection.png' alt='drawing' width='625'/>

Play through the game to exploit the secure host and capture the flag by executing the correct kill chain.


## Running CybORG
Below is the bare minimum of getting CybORG to run with a `Red` agent and a `Scenario1.yaml` file. Please change the relevant fields for your specific needs. 
```python
from CybORG import CybORG
import inspect
path = str(inspect.getfile(CybORG))
path = path[:-10] + '/Shared/Scenarios/Scenario1.yaml'
env = CybORG(path, 'sim')
env.reset(agent='Red')

steps = 1000
for eps in range(1):
    print(f'Game: {eps}')
    env.start(steps)
    env.reset() 
```

### Wrappers
Running CybORG can also be done under the use of our wrappers. These wrappers puts an overlay over the CybORG code to retrofit it to conform to other design requirements. Here is an example of using the various wrappers to get CybORG to function in an OpenAI Gym environment. 

```python
import inspect
from CybORG import CybORG
from CybORG.Agents.Wrappers.OpenAIGymWrapper import OpenAIGymWrapper
from CybORG.Agents.Wrappers.IntListToAction import IntListToActionWrapper
from CybORG.Agents.Wrappers.FixedFlatWrapper import FlatFixedWrapper

agent = 'Red'
path = str(inspec.getfile(CybORG))
path = path[:-10] + '/Shared/Scenarios/Scenario1.yaml'
cyborg = OpenAIGymWrapper(IntListToActionWrapper(FlatFixedWrapper(CybORG(path, 'sim'))),  agent)

cyborg.reset()
episode = 100
for i in range(episode):
    action = cyborg.action_space.sample()
    obs, rew, done, info - cyborg.step(action)

    if done:
        print('Episode finished after {} timesteps'.format(i+1))
        break

```

## CybORG Observation Design
The observation space in CybORG is set up as a complex diciontary data structure that also features methods to add information to this data structure. 

The dictionary always contains a trinary value for a `success` entry to represent successful, unsuccessful or unknown and indicates the result of the action.

The other keys within the observation are host IDS and are string values that are used to collect information that is believed to originate from a single host. Examples of host IDS are IP addresses or hostnames. These host ID is not guaranteed to repesent any information and for the agents to infer information from the environment. 

Each host ID also has a dictionary structure within that has information collected about the host. These entries have keys:
* Sessions
* Files
* Interfaces
* Proccesses
* Users
* System Info

An example of the observation data structure are show here:
```python
{
    "Success": TrinaryEnum,
    "<hostid0>" : {
        "Processes":[
            {"PID": int
            "PPID": int
            "Process name": str
            "Known Process": ProcessNameEnum
            "Program name": FileNameEnum
            "Username": str
            "Path": str
            "Known Path": PathEnum
            "Connections": {
                "Local Port": int
                "Local Address": IPv4Address
                "Remote port": int
                "Remote Address": IPv4Address
                "Application Protocol": ApplicationProtocolEnum,
                "Transport Protocol": TransportProtocolEnum
            }  
            "status": ProcessStateEnum
            "type": ProcessTypeEnum
            "version": ProcessVersionEnum
            "vulnerabilities": [VulnerabilityEnum]},
            ...
        ],
        "System info":{
                "Hostname": str,
                "OSType": OperatingSystemTypeEnum,
                "OSDistribution": OperatingSystemDistributionEnum,
                "OSVersion": OperatingSystemVersionEnum,
                "OSKernelVersion": OperatingSystemKernelVersionEnum,
                "Patches": [OperatingSystemPatchEnum],
                "Architecture": ArchitectureEnum
        },
        "Interfaces":[
            {"Interface": str,
            "IP Address" : IPv4Address,
            "Subnet": IPv4Subnet
            },
            ...
        ],
        "User Info":[
                {"Username" : str,
                "UID" : int,
                "Password" : str,
                "Password Hash" : str,
                "Password Hash Type" : PasswordHashType,
                "Groups" : [
                    {"Group Name" : str,
                     "Builtin Group" : BuiltInGroupsEnum,
                     "GID" : int},
                    ...
                ]
            },
            ...
        ],
        "Files":[
            {"Name" : str,
            "Known File" : FileNameEnum,
            "Group" : str,
            "GroupPermission" : int,
            "User" : str,
            "UserPermission" : int,
            "DefaultPermission" : int
            "Path": str,
            "Known Path": PathEnum,
            "Vendor": VendorEnum,
            "Version": str
            },
            ...
        ],
        "Sessions": [
            {"ID": int,
            "Username": str,
            "Timeout": int,
            "PID": int,
            "Type": SessionTypeEnum,
            "Agent": str}
        ]      
    }
...
}
```

While, an example of how an action will utilise the necessary keys within this data structure format can be shown below. This example shows a successful port scan and revealing an open port.

```python
{"success": True,
str(scanned_ip_address): {
	'Interface': [{'IP Address': scanned_ip_address}],
	'Processes': [{
		'Connections': [{
			'Local Address': scanned_ip_address,
			'Local Port': 22}]}]},
}
```
